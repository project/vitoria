## CONTENTS OF THIS FILE

- Introduction
- Installation
- Configuration

## INTRODUCTION

vitoria is a modern and versatile Drupal theme designed for drupal smart store commerce. a.

* For a full description of the theme, visit the project page: https://www.drupal.org/project/vitoria

* To submit bug reports and feature suggestions, or track changes: https://www.drupal.org/project/issues/vitoria

## INSTALLATION

- Install as you would normally install a contributed Drupal theme. Visit https://www.drupal.org/node/1897420 for further information.

## CONFIGURATION

The vitoria theme offers various configuration options to customize its appearance. To access the theme settings:

1. Log in to your Drupal administration panel.
2. Go to Appearance in the admin menu.
3. Find the vitoria theme and click on the Settings link.


