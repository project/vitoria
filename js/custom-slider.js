(function ($, Drupal, window, document, undefined) {
  Drupal.behaviors.custom_slider_vitoria_js = {
    attach: function (context, settings) {
      //Home Page Banner Slider Start
      if (jQuery(".banner-slider").length) {
        jQuery(".banner-slider").not('.slick-initialized').slick({
          infinite: true,
          speed: 800,
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: true,
          autoplay: true,
          autoplaySpeed: 3000,
        });
      }
      //Home Page Banner Slider End
      //Home Page Hero Slider Start
      if (jQuery(".show1 .view-content ul").length) {
        jQuery(".show1 .view-content ul").not('.slick-initialized').slick({
          infinite: true,
          speed: 800,
          slidesToShow: 1,
          slidesToScroll: 1,
          autoplay: true,
          autoplaySpeed: 1500,
          responsive: [
            {
              breakpoint: 900,
              settings: {
                slidesToShow: 1,
              },
            },
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 1,
              },
            }
          ],
        });
      }
      //Home Page Hero Slider End


      // brand slider start
      if (jQuery(".show7 .view-content ul").length) {
        jQuery(".show7 .view-content ul").not('.slick-initialized').slick({
          infinite: true,
          speed: 800,
          slidesToShow: 7,
          slidesToScroll: 1,
          autoplay: true,
          autoplaySpeed: 900,
          arrows: false,
          responsive: [
            {
              breakpoint: 900,
              settings: {
                slidesToShow: 4,
              },
            },
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 2,
              },
            }
          ],
        });
      }
      // brand slider end

      // Product slider Start

      if (jQuery(".show5 .view-content ul").length) {
        jQuery(".show5 .view-content ul").not('.slick-initialized').slick({
          infinite: false,
          speed: 800,
          slidesToShow: 5,
          slidesToScroll: 1,
          arrows: true,
          centerMode: false,
          responsive: [
            {
              breakpoint: 900,
              settings: {
                slidesToShow: 4,
              },
            },
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 1,
              },
            }
          ],
        });
      }
      // Product slider End
      if (jQuery(".imagezoom-slider ul").length) {
        jQuery(".imagezoom-slider ul").not('.slick-initialized').slick({
          infinite: true,
          speed: 800,
          slidesToShow: 5,
          slidesToScroll: 1,
          arrows: true,
          responsive: [
            {
              breakpoint: 900,
              settings: {
                slidesToShow: 4,
              },
            },
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 2,
              },
            }
          ],
        });
      }
    }
  }
})(jQuery, Drupal, this, this.document);